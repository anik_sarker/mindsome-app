angular.module('starter.services', [])
.factory('Mindful', function($rootScope) {
	var mindfuldays = 0;    
	var result = {
		increment: function() {
			mindfuldays++;
            console.log('Mindful Days: ' + mindfuldays.toString());
		},
		decrement: function() {
            if (mindfuldays > 0) {
                mindfuldays--;
            } else {
                mindfuldays = 0
            }
            console.log('Mindful Days: ' + mindfuldays.toString());
		},
		get: function() {
            console.log('Capturando dias: ' + mindfuldays.toString());
			return mindfuldays;
		}
	}
	$rootScope.$watch(function () {
		return mindfuldays;
	}, function (value) {
		$rootScope.$broadcast('MindfulSrv:mindfuldays', value);
	}, true);

	return result;
});
 