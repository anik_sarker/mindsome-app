angular.module('starter.controllers', ['ngCordova', 'angular-svg-round-progress', 'starter.directives', 'starter.services'])


// App Controller
.controller('AppCtrl', function ($scope, $ionicModal, $timeout, $location, $rootScope) {

    // $scope variables

    $scope.lgCircle             = {};
    $scope.lgCircle.desc        = 'Mindful Days';
    $scope.lgCircle.mindfuldays = 0;
    $rootScope.mindfuldays      = 0;

    $scope.smCircles = [
        {'heading':"3",'desc':"Level", 'color':"red"},
        {'heading':"365",'desc':"Challenges"},
        {'heading':"75/100",'desc':"Points"}
    ];
    $scope.username = "Jimmy";
    $scope.greetingMessage = "Welcome Back"
    
    /*************************
     * NEW ARRAY:
     *
     * Packages
     *    |
     *    +-- Challenges/Paths
     *            |
     *            +-- Days
     *
     * Example: Starter Pack 1 -> Challenge Nr. 1 (Use Stairs) -> Day 1, 2 and 3.
     *
     *************************/
    
    $scope.packages = [
        {id: 1, name: 'Starter Pack 1', color: '#79c6c4', logo: '11.svg',
         challenges: [{id: 1, headline: 'Challenge Nr. 1 - Use Stairs', subline:'Mental Wellbeing', logo: 'wave.svg',
                       desc: 'For a brief and easy exercise in mindfulness, take a minute to focus on your breathing and count twelve breaths',
                       longdesc: 'For a brief and easy exercise in mindfulness, take a minute to focus on your breathing and count twelve breaths. For a brief and easy exercise in mindfulness, take a minute to focus on your breathing and count twelve breaths. For a brief and easy exercise in mindfulness, take a minute to focus on your breathing and count twelve breaths.',
                       active: false,
                       skipped: false,
                       enabled: false,
                       done: false,
                       days: [{id: 1, points: 5, done: false},
                              {id: 2, points: 5, done: false},
                              {id: 3, points: 5, done: false}]
                      },
                      {id: 2, headline: 'Challenge Nr. 2', subline: 'Physical Fitness', logo: 'watch.svg',
                       desc: 'Details about the activity',
                       longdesc: 'More details about the activity',
                       active: false,
                       skipped: false,
                       enabled: false,
                       done: false,
                       days: [{id: 1, points: 5, done: false},
                              {id: 2, points: 5, done: false},
                              {id: 3, points: 5, done: false},
                              {id: 4, points: 10, done: false},
                              {id: 5, points: 10, done: false},
                              {id: 6, points: 10, done: false}]
                      },
                      {id: 3, headline: 'Challenge Nr. 3', subline: 'Subline 3', logo: 'wave.svg',
                       desc: 'Details about the activity',
                       longdesc: 'More details about the activity',
                       active: false,
                       skipped: false,
                       enabled: false,
                       done: false,
                       days: [{id: 1, points: 5, done: false},
                              {id: 2, points: 5, done: false},
                              {id: 3, points: 5, done: false},
                             ]
                      },
                      {id: 4, headline: 'Challenge Nr. 4', subline: 'Subline 2', logo: 'watch.svg',
                       desc: 'Details about the activity',
                       longdesc: 'More details about the activity',
                       active: false,
                       skipped: false,
                       enabled: false,
                       done: false,
                       days: [{id: 1, points: 5, done: false},
                              {id: 2, points: 5, done: false},
                              {id: 3, points: 5, done: false}]
                      },
                      {id: 5, headline: 'Challenge Nr. 5', subline: 'Subline 1', logo: 'wave.svg',
                       desc: 'Details about the activity',
                       longdesc: 'More details about the activity',
                       active: false,
                       skipped: false,
                       enabled: false,
                       done: false,
                       days: [{id: 1, points: 5, done: false},
                              {id: 2, points: 5, done: false},
                              {id: 3, points: 5, done: false}]
                      }
                     ]
        },
        {id: 2, name: 'Starter Pack 2', color: '#a1d6d9', logo: '22.svg',
         challenges: [{id: 1, headline: 'Challenge Nr. 6', subline: 'Mental Wellbeing',    logo: 'wave.svg',   desc: '', longdesc: '', active: false, skipped: false, enabled: false, done: false, days: []},
                      {id: 2, headline: 'Challenge Nr. 7', subline: 'Physical Fitness',    logo: 'watch.svg',  desc: '', longdesc: '', active: false, skipped: false, enabled: false, done: false, days: []},
                      {id: 3, headline: 'Challenge Nr. 8', subline: 'Subline 3',           logo: 'wave.svg',   desc: '', longdesc: '', active: false, skipped: false, enabled: false, done: false, days: []},
                      {id: 4, headline: 'Challenge Nr. 9', subline: 'Subline 2',           logo: 'watch.svg',  desc: '', longdesc: '', active: false, skipped: false, enabled: false, done: false, days: []},
                      {id: 5, headline: 'Challenge Nr. 10', subline: 'Subline 1',          logo: 'wave.svg',   desc: '', longdesc: '', active: false, skipped: false, enabled: false, done: false, days: []}]
        },
        {id: 3, name: 'Mindful Eating', color: '#f5aca6', logo: '33.svg',
         challenges: [{id: 1, headline: 'Challenge Nr. 11', subline: 'Mental Wellbeing',   logo: 'wave.svg',   desc: '', longdesc: '', active: false, skipped: false, enabled: false, done: false, days: []},
                      {id: 2, headline: 'Challenge Nr. 12', subline: 'Physical Fitness',   logo: 'watch.svg',  desc: '', longdesc: '', active: false, skipped: false, enabled: false, done: false, days: []},
                      {id: 3, headline: 'Challenge Nr. 13', subline: 'Subline 3',          logo: 'wave.svg',   desc: '', longdesc: '', active: false, skipped: false, enabled: false, done: false, days: []},
                      {id: 4, headline: 'Challenge Nr. 14', subline: 'Subline 2',          logo: 'watch.svg',  desc: '', longdesc: '', active: false, skipped: false, enabled: false, done: false, days: []},
                      {id: 5, headline: 'Challenge Nr. 15', subline: 'Subline 1',          logo: 'wave.svg',   desc: '', longdesc: '', active: false, skipped: false, enabled: false, done: false, days: []}]
        },
        {id: 4, name: 'Relationships', color: '#fad0cf', logo: '44.svg',
         challenges: [{id: 1, headline: 'Challenge Nr. 16', subline: 'Mental Wellbeing',   logo: 'wave.svg',   desc: '', longdesc: '', active: false, skipped: false, enabled: false, done: false, days: []},
                      {id: 2, headline: 'Challenge Nr. 17', subline: 'Physical Fitness',   logo: 'watch.svg',  desc: '', longdesc: '', active: false, skipped: false, enabled: false, done: false, days: []},
                      {id: 3, headline: 'Challenge Nr. 18', subline: 'Subline 3',          logo: 'wave.svg',   desc: '', longdesc: '', active: false, skipped: false, enabled: false, done: false, days: []},
                      {id: 4, headline: 'Challenge Nr. 19', subline: 'Subline 2',          logo: 'watch.svg',  desc: '', longdesc: '', active: false, skipped: false, enabled: false, done: false, days: []},
                      {id: 5, headline: 'Challenge Nr. 20', subline: 'Subline 1',          logo: 'wave.svg',   desc: '', longdesc: '', active: false, skipped: false, enabled: false, done: false, days: []}]
        },
        {id: 5, name: 'Mindful Exercising', color: '#a6bbe2', logo: '55.svg',
         challenges: [{id: 1, headline: 'Challenge Nr. 21', subline: 'Mental Wellbeing',   logo: 'wave.svg',   desc: '', longdesc: '', active: false, skipped: false, enabled: false, done: false, days: []},
                      {id: 2, headline: 'Challenge Nr. 22', subline: 'Physical Fitness',   logo: 'watch.svg',  desc: '', longdesc: '', active: false, skipped: false, enabled: false, done: false, days: []},
                      {id: 3, headline: 'Challenge Nr. 23', subline: 'Subline 3',          logo: 'wave.svg',   desc: '', longdesc: '', active: false, skipped: false, enabled: false, done: false, days: []},
                      {id: 4, headline: 'Challenge Nr. 24', subline: 'Subline 2',          logo: 'watch.svg',  desc: '', longdesc: '', active: false, skipped: false, enabled: false, done: false, days: []},
                      {id: 5, headline: 'Challenge Nr. 25', subline: 'Subline 1',          logo: 'wave.svg',   desc: '', longdesc: '', active: false, skipped: false, enabled: false, done: false, days: []}]
        },
        {id: 6, name: 'Work-Life-Balance', color: '#d3e1f5', logo: '11.svg',
         challenges: [{id: 1, headline: 'Challenge Nr. 26', subline: 'Mental Wellbeing',   logo: 'wave.svg',   desc: '', longdesc: '', active: false, skipped: false, enabled: false, done: false, days: []},
                      {id: 2, headline: 'Challenge Nr. 27', subline: 'Physical Fitness',   logo: 'watch.svg',  desc: '', longdesc: '', active: false, skipped: false, enabled: false, done: false, days: []},
                      {id: 3, headline: 'Challenge Nr. 28', subline: 'Subline 3',          logo: 'wave.svg',   desc: '', longdesc: '', active: false, skipped: false, enabled: false, done: false, days: []},
                      {id: 4, headline: 'Challenge Nr. 29', subline: 'Subline 2',          logo: 'watch.svg',  desc: '', longdesc: '', active: false, skipped: false, enabled: false, done: false, days: []},
                      {id: 5, headline: 'Challenge Nr. 30', subline: 'Subline 1',          logo: 'wave.svg',   desc: '', longdesc: '', active: false, skipped: false, enabled: false, done: false, days: []}]
        }
    ];

    /***********************
     * Functions
     ***********************/

    $scope.go = function (path) {
      $location.path(path);
    };
    
    // Check if package can be played
    $scope.checkPackages = function () {
        var lEnable = true;
        if ($scope.packages.length > 0) {
            for (var x = 0; x < $scope.packages.length; x++) {
                lEnable = $scope.checkChallenges(x, lEnable);
            }
        }
    }

    $scope.checkChallenges = function (nPack, lEnable) {
        if ($scope.packages[nPack].challenges.length > 0) {
            for (var x = 0; x < $scope.packages[nPack].challenges.length; x++) {
                $scope.packages[nPack].challenges[x].enabled = lEnable;
                if (!$scope.packages[nPack].challenges[x].skipped) {
                    if ($scope.packages[nPack].challenges[x].days.length > 0) {
                        for (var y = 0; y < $scope.packages[nPack].challenges[x].days.length; y++) {
                            if (!$scope.packages[nPack].challenges[x].days[y].done) {
                                lEnable = false;
                            }
                        }
                    }
                    $scope.packages[nPack].challenges[x].done = lEnable;
                }
            }
        }
        return lEnable;
    }
    // Generic Modal
    $ionicModal.fromTemplateUrl('templates/modal.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal) {
        $scope.modal = modal;
    });

    $scope.openModal = function(title, text) {
        $scope.modal.title = title;
        $scope.modal.text  = text;
        $scope.modal.show()
    }
    $scope.closeModal = function() {
        $scope.modal.hide();
    };
    $scope.$on('$destroy', function() {
        $scope.modal.remove();
    });
})

// Start (login)
.controller('StartCtrl', function ($scope, $location) {
    $scope.intro = function () {
      $location.path('/intro');
    };
})

// Intro
.controller('IntroCtrl', function ($scope, $location) {
    $scope.start = function () {
      $location.path('/app/home');
    };
})

// Home
.controller('HomeCtrl', function ($scope, $rootScope, Mindful) {
    // Broadcast Watcher
    $rootScope.$on('MindfulSrv:mindfuldays', function (event, value) {
        $scope.lgCircle.mindfuldays = value;
        if (value == 1) {
            $scope.lgCircle.desc  = 'Mindful Day';
        } else {
            $scope.lgCircle.desc  = 'Mindful Days';
        }
            
    });
})

// Paths
.controller('PathCtrl', function ($scope, $stateParams, $location, $ionicModal) {
    var nPackage   = $stateParams.pathId;
    
    $scope.paths = $scope.packages[nPackage].challenges;
    
    $scope.selectedPackage = nPackage;
    $scope.checkPackages(); // Check if challenge can be played
    
    $scope.openChallenge = function(nPackage, nChallenge) {
        if ($scope.paths[nChallenge].enabled) {
            $location.path('/app/package/' + nPackage + '/challenge/' + nChallenge);
        } else {
            $scope.openModal('Wait!', 'Please Finish or Skip the Challenge Before First.');
        }
    }
})

// Challenges
.controller('ChallengeCtrl', function ($scope, $stateParams, $ionicPlatform, $ionicHistory, $ionicPopover, $ionicScrollDelegate, $rootScope, Mindful) {
        var nPackage   = $stateParams.pathId;
        var nChallenge = $stateParams.challengeId;
 
        $scope.paths            = $scope.packages[nPackage].challenges;
        $scope.challenge        = $scope.paths[nChallenge];
        $scope.showlongdesc     = false;
        $scope.currentpackage   = $scope.packages[nPackage].id;

        // Start Challenge
        $scope.countDays = function () {
            var lShowDays   = false;
            var cStatus     = '';
            var nAbsolved   = 0;
            var nRemaining  = 0;
            var nPtTotal    = 0;
            var nPtDone     = 0;
            var nDays       = $scope.paths[nChallenge].days.length;
            
            // Count the points of the challenge
            if (nDays > 0) {
              for (var i = 0; i < nDays; i++) {
                if ($scope.paths[nChallenge].days[i].done) {
                  nAbsolved++;
                  nPtDone += $scope.paths[nChallenge].days[i].points;
                } else {
                  nRemaining++;
                }
                nPtTotal += $scope.paths[nChallenge].days[i].points;
              }
              lShowDays = true;
            }
            
            // Define the status text
            cStatus = nAbsolved.toString();
            if (nAbsolved == 1) {
              cStatus = cStatus + ' day absolved';
            } else {
              cStatus = cStatus + ' days absolved';
            }
            cStatus = cStatus + ' - ' + nRemaining.toString();
            if (nRemaining == 1) {
              cStatus = cStatus + ' day remaining';
            } else {
              cStatus = cStatus + ' days remaining';
            }
        
            $scope.absolved     = nAbsolved;
            $scope.remaining    = nRemaining;
            $scope.pts_total    = nPtTotal;
            $scope.pts_done     = nPtDone;
            $scope.status       = cStatus;
            $scope.showdays     = lShowDays;
            $scope.days         = (nDays == 1)?'1 Day':nDays.toString() + ' Days'; 
        }
        
        /***********************
         * Functions
         ***********************/
    
        // Start Challenge
        $scope.start = function () {
            $scope.paths[nChallenge].active = true;
            $scope.paths[nChallenge].skipped = false;
            $ionicScrollDelegate.resize();
        }
    
        // Skip Challenge
        $scope.skip = function () {
            $scope.paths[nChallenge].skipped = true;
            $scope.checkPackages();
            $ionicHistory.goBack();
        }

        // Cancel Challenge
        $scope.cancel = function () {
            $scope.paths[nChallenge].active = false;
            $scope.paths[nChallenge].skipped = false;
            for (var i = 0; i < $scope.paths[nChallenge].days.length; i++) {
                if ($scope.paths[nChallenge].days[i].done) {
                    $scope.paths[nChallenge].days[i].done = false;
                    Mindful.decrement();
                }
            }
            $scope.checkPackages();
            $scope.countDays();
        }

        // Done Challenge
        $scope.done = function () {
            $scope.paths[nChallenge].done = true;
            $scope.paths[nChallenge].skipped = false;
            for (var i = 0; i < $scope.paths[nChallenge].days.length; i++) {
                if (!$scope.paths[nChallenge].days[i].done) {
                    $scope.paths[nChallenge].days[i].done = true;
                    Mindful.increment();
                }
            }
            $scope.checkPackages();
            $scope.countDays();
        }
    
        // Play Day
        $scope.play = function(day, $event) {
            if(!day.done) {
                day.done = true;
                Mindful.increment();
                $scope.checkPackages();
                $scope.countDays();
                if($scope.remaining == 0){
                    $scope.openPopover($event);
                }
            }
        }

        // Undo
        $scope.unplay = function(day) {
            if(day.done) {
                day.done = false;
                Mindful.decrement();
                $scope.checkPackages();
                $scope.countDays();
            }
        }
        
        // Broadcast Watcher
        $rootScope.$on('MindfulSrv:mindfuldays', function (event, value) {
            $rootScope.mindfuldays = value;
        });
    
        /***********************
         * Popover
         ***********************/
        $ionicPopover.fromTemplateUrl('templates/popoverShare.html', {
            scope: $scope
        }).then(function(popover) {
            $scope.popover = popover;
        })
        $scope.openPopover = function($event) {
            $scope.popover.show($event);
        };
        $scope.closePopover = function() {
            $scope.popover.hide();
        };
        $scope.$on('$destroy', function() {
            $scope.popover.remove();
        });
        
        $scope.countDays();
});
