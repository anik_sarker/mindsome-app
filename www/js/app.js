// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers', 'ngCordova'])

  .run(function ($ionicPlatform) {
    $ionicPlatform.ready(function () {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      if (window.cordova && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        cordova.plugins.Keyboard.disableScroll(true);

      }
      if (window.StatusBar) {
        // org.apache.cordova.statusbar required
        StatusBar.styleDefault();
      }
    });
  })

  .config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('app', {
        url: '/app',
        abstract: true,
        views: {
          'content': {
            templateUrl: 'templates/menu.html',
            controller: 'AppCtrl'
          }
        }
      })    
      .state('start', {
        url: '/start',
        views: {
          'content': {
            templateUrl: 'templates/start.html',
            controller: 'StartCtrl'
          }
        }
      })
      .state('intro', {
        url: '/intro',
        views: {
          'content': {
            templateUrl: 'templates/intro.html',
            controller: 'IntroCtrl'
          }
        }
      })
      .state('app.home', {
        url: '/home',
        views: {
          'menuContent': {
            templateUrl: 'templates/home.html',
            controller: 'HomeCtrl'
          }
        }
      })
      .state('app.package', {
        url: '/package/:pathId',
        views: {
          'menuContent': {
            templateUrl: 'templates/path.html',
            controller: 'PathCtrl'
          }
        }
      })
      .state('app.challenge', {
        url: '/package/:pathId/challenge/:challengeId',
        views: {
          'menuContent': {
            templateUrl: 'templates/challenge.html',
            controller: 'ChallengeCtrl'
          }
        }
      });
      /*.state('app.package.challenge', {
        url: '/challenge/:challengeId',
        views: {
          'menuContent': {
            templateUrl: 'templates/challenge.html',
            controller: 'ChallengeCtrl'
          }
        }
      });*/
    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/start');
  });
