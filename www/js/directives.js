angular.module('starter.directives', [])
.directive('lgCircle', function() {
    var directive = {};

    directive.restrict = 'EA';
    directive.templateUrl = "templates/lgCircle.html";

    directive.scope = {
        circle : "=circle"
    }

    return directive;
})

.directive('smCircle', function() {
    var directive = {};

    directive.restrict = 'EA';
    directive.templateUrl = "templates/smCircle.html";

    directive.scope = {
        circle : "=circle"
    }

    return directive;
});
